<?php

namespace App\Listeners;
use Illuminate\Support\Facades\Redis;

use App\Events\ExampleEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEventNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(ExampleEvent $event)
    {
        \Log::info('event handle');
        // Redis::subscribe(['test-event'], function ($message) {
        //     var_dump('message'.$message);
        // });

    }
}
