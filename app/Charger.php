<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Charger extends Model
{   

    public $timestamps = true;
    
	use Notifiable;

    protected $fillable = [
    	'cid', 
    	'is_belong_to', 
        'located_at',
        'status',
        'device_id',
        'updated_at',
        'created_at'
    ];

    protected $hidden = [
    	'remember_token',
    ];

    public function group()
    {
        return $this->belongsTo('App\Group','is_belong_to');
    }

  }
