<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use App\Socket;
use App\Charger;


class HomeController extends Controller
{
  public function index(Request $request){

    if(!\Auth::user()){
      return redirect()->route('login');
    }

    $user = \Auth::user();
    $chargers = Charger::where('is_belong_to', $user->id)->get();


    return response()->view('home', ['chargers'=> $chargers] , 200);
  }

  public function login(){
    return response()->view('auth.login');
  }

  public function api(Request $request){
    return response()->view('serial');

  }

  private function handle_client($ssock, $csock) 
  { 
    Log::info("handle_client: new socket is here"); 
    GLOBAL $__server_listening; 

    $pid = pcntl_fork(); 

    if ($pid == -1) 
    { 
      /* fork failed */ 
      echo "fork failure!\n"; 
      die; 
    }elseif ($pid == 0) 
    { 
      /* child process */ 
      $__server_listening = false; 
      socket_close($ssock); 
      socket_close($csock); 
    }else 
    { 
      socket_close($csock); 
    } 
  } 

  private function connect($socket){
    array_push($sockets,$socket);
    \Log::info($socket." CONNECTED!");
  }

  private function disconnect($socket){
    $found=null;

    $n=count($users);

    for($i=0;$i<$n;$i++){
      if($users[$i]->socket==$socket){ $found=$i; 
        break; 
      }
    }

    if(!is_null($found)){ 
      array_splice($users,$found,1); 
    }

    $index = array_search($socket,$sockets);
    socket_close($socket);

    \Log::info($socket." DISCONNECTED!");
    if($index>=0){ 
      array_splice($sockets,$index,1); 
    }
  }

  private function dohandshake($user,$buffer){

    \Log::info("\nRequesting handshake...");
    \Log::info($buffer);

    list($resource,$host,$origin,$strkey1,$strkey2,$data) = getheers($buffer);
    \Log::info("Handshaking...");

    $pattern = '/[^\d]*/';
    $replacement = '';
    $numkey1 = preg_replace($pattern, $replacement, $strkey1);
    $numkey2 = preg_replace($pattern, $replacement, $strkey2);

    $pattern = '/[^ ]*/';
    $replacement = '';
    $spaces1 = strlen(preg_replace($pattern, $replacement, $strkey1));
    $spaces2 = strlen(preg_replace($pattern, $replacement, $strkey2));

    if ($spaces1 == 0 || $spaces2 == 0 || $numkey1 % $spaces1 != 0 || $numkey2 % $spaces2 != 0) {
      socket_close($user->socket);
      \Log::info('failed');
      return false;
    }

    $ctx = hash_init('md5');
    hash_update($ctx, pack("N", $numkey1/$spaces1));
    hash_update($ctx, pack("N", $numkey2/$spaces2));
    hash_update($ctx, $data);
    $hash_data = hash_final($ctx,true);

    $upgrade  = "HTTP/1.1 101 WebSocket Protocol Handshake\r\n" .
    "Upgrade: WebSocket\r\n" .
    "Connection: Upgrade\r\n" .
    "Sec-WebSocket-Origin: " . $origin . "\r\n" .
    "Sec-WebSocket-Location: ws://" . $host . $resource . "\r\n" .
    "\r\n" .
    $hash_data;

    socket_write($user->socket,$upgrade.chr(0),strlen($upgrade.chr(0)));
    $user->handshake=true;
    \Log::info($upgrade);
    \Log::info("Done handshaking...");
    return true;
  }


  private function getheaders($req){
    $r=$h=$o=null;
    if(preg_match("/GET (.*) HTTP/"   ,$req,$match)){ $r=$match[1]; }
    if(preg_match("/Host: (.*)\r\n/"  ,$req,$match)){ $h=$match[1]; }
    if(preg_match("/Origin: (.*)\r\n/",$req,$match)){ $o=$match[1]; }
    if(preg_match("/Sec-WebSocket-Key2: (.*)\r\n/",$req,$match)){ $key2=$match[1]; }
    if(preg_match("/Sec-WebSocket-Key1: (.*)\r\n/",$req,$match)){ $key1=$match[1]; }
    if(preg_match("/\r\n(.*?)\$/",$req,$match)){ $data=$match[1]; }
    return array($r,$h,$o,$key1,$key2,$data);
  }

  private function getuserbysocket($socket){
    global $users;
    $found=null;
    foreach($users as $user){
      if($user->socket==$socket){ $found=$user; break; }
    }
    return $found;
  }

}
