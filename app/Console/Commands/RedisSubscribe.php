<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Events\ExampleEvent;



class RedisSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    public function handle()
    {

      Redis::psubscribe(['*'], function ($message, $channel) {
        
        // $req = explode('/', $message); 

        // for($i=0; $i<3; $i++){
        //   var_dump($req[$i]);
        // }
        \DB::table('chargers')->updateOrInsert(
          ['channel' => $channel , 'cid' => '00001'] );

        if($message == 'S0'){
          \DB::table('chargers')->where('channel', $channel)->where('cid','00001')
          ->update(['status' => 0]);
        }
        if($message == 'S1'){
          \DB::table('chargers')->where('channel', $channel)->where('cid','00001')
          ->update(['status' => 1]);
        }
        if($message == 'S2'){
          \DB::table('chargers')->where('channel', $channel)->where('cid','00001')
          ->update(['status' => 2]);
        }
        if($message == 'S3'){
          \DB::table('chargers')->where('channel', $channel)->where('cid','00001')
          ->update(['status' => 3]);
        }

        $updated = json_encode(['message' => $message , 'channel' => $channel]);
        \DB::table('logs')->insert(['charger_id' => '1', 'message' => $message, 'channel' => $channel]);
        event(new ExampleEvent());
      });


      \DB::table('logs')->insert(['charger_id' => '1', 'message' => 'event get', 'channel' => $channel]);


    }
  }
